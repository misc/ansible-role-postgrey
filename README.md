# Ansible role for Postgrey installation

## Introduction

[Postgrey](http://postgrey.schweikert.ch/) is a Postfix policy server
implementing greylisting.

This role installs and configure the server.

You can specify whitelisted clients.

## Variables

- **whitelist_clients**: site-specific whitelisted clients

## TODO

More daemon options could be interesting (/etc/sysconfig/postgrey).

